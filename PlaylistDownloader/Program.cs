﻿using HtmlAgilityPack;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace PlaylistDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            using (StreamWriter writer = new StreamWriter("out.txt"))
            {
                Console.SetOut(writer);

                var doc = new HtmlDocument();
                doc.Load("data.txt", Encoding.UTF8);
                var nodes = doc.DocumentNode.Descendants("span")
                    .Where(x => x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("song-play"))
                    .Select(y => new { Url = y.Attributes["data-url"].Value, Title = y.Attributes["data-title"].Value })
                    .ToList();

                Directory.CreateDirectory("Music");
                Console.WriteLine($"Songs count: {nodes.Count}");
                var songNumber = 0;

                foreach (var link in nodes)
                {
                    Console.WriteLine($"{++songNumber} {link.Title}");
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile($"https://zf.fm/{link.Url}", $"Music/{link.Title}.mp3");
                    }
                }

                Console.WriteLine("Download Finished");
            }
        }
    }
}
